
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  State<InitialScreen> createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        //Navigator.of(context).pushNamed("/");
      }
    });

    return SafeArea(
        child: Scaffold(
            backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
            body: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/init.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text("Bidit",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            height: 3,
                            color: Color.fromRGBO(175, 188, 203, 1)),
                        textAlign: TextAlign.center),
                    const Text(
                      "Get the clothes you’re looking for",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 35,
                          height: 1.5,
                          color: Color.fromRGBO(20, 20, 20, 1)),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                        alignment: Alignment.bottomCenter,
                        height:300,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(30)),
                                  backgroundColor: Color.fromRGBO(250, 250, 250, 1),
                            ),
                            onPressed: () {
                              onPressed(context);
                            },
                            child: const Text('Get started',
                            style: TextStyle(
                              color: Color.fromRGBO(20, 20, 20, 1)
                            ),
                            )))
                  ]),
            )));
  }

  void onPressed(BuildContext context) async {
    Navigator.of(context).pushNamed("/login");
    
  }
}

