import 'package:bidit/controller/analytics_controller.dart';
import 'package:bidit/controller/auth_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/controller/prenda_controller.dart';
import 'package:bidit/controller/tienda_controller.dart';
import 'package:bidit/controller/total_controller.dart';
import 'package:bidit/controller/wishlist_controller.dart';
import 'package:bidit/model/prenda_model.dart';
import 'package:bidit/model/wishlist_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class ProfileAuctionScreen extends StatefulWidget {
  @override
  _ProfileAuctionScreenState createState() =>
      _ProfileAuctionScreenState(prendaId: prendaId);

  const ProfileAuctionScreen({Key? key, required this.prendaId}) : super(key: key);

  final String prendaId;
}

class _ProfileAuctionScreenState extends State<ProfileAuctionScreen> {
  _ProfileAuctionScreenState({required this.prendaId});

  final String prendaId;

  PrendaController prendaController = PrendaController();
  TiendaController tiendaController = TiendaController();
  AnalyticsController analyticsController = AnalyticsController();
  TextEditingController bidController = TextEditingController();
  AuthController authController = AuthController();
  TotalController totalController = TotalController();
  WishlistController wishlistController = WishlistController();
  String prendaImagen = "";

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  int timestampActual = (DateTime.now().millisecondsSinceEpoch / 1000).round();

  void initState() {
    super.initState();
    print("Prenda auction");
  }

  Future<Prenda> getCloth() async {
    return await prendaController.getOne(prendaId);
  }

  void trackScreen() {
    analyticsController.trackScreen('detailClothes', 'ProfileAuctionScreen');
  }

  void bidAuction() async {
    final ConnectivityController connectivityController =
    ConnectivityController(); // Create an instance of ConnectivityController
    bool isConnected = await connectivityController.isConnectedToInternet();
    final FormState? form = _formKey.currentState;
    if (!isConnected) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection', style: TextStyle(color: Colors.white)),
            content: Text('Please try again later...', style: TextStyle(color: Colors.white)),
            backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
            actions: <Widget>[
              TextButton(
                child: Text('OK', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    } else {
      if (form != null && form.validate()) {
        Prenda aux = await prendaController.getOne(prendaId);
        String? userId = await authController.getUserId();
        aux.setPrice(int.parse(bidController.text));
        aux.setUserRef(userId);
        prendaController.update(prendaId, aux);
        totalController.update();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("You bidit succesfully!", style: TextStyle(color: Colors.white)),
          backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
        ));
        Navigator.pop(context);
      } else {
        print('Form is invalid');
      }
    }
  }

  void addWishList() async {
    final ConnectivityController connectivityController =
    ConnectivityController(); // Create an instance of ConnectivityController
    bool isConnected = await connectivityController.isConnectedToInternet();
    final FormState? form = _formKey.currentState;
    if (!isConnected) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection', style: TextStyle(color: Colors.white)),
            content: Text('Please try again later...', style: TextStyle(color: Colors.white)),
            backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
            actions: <Widget>[
              TextButton(
                child: Text('OK', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    } else {
      String? userId = await authController.getUserId();
      Wishlist aux = Wishlist(userRef: userId ?? '', clothesRef: prendaId);
      wishlistController.add(aux);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: const Text("Item added to your wishlist"),
    ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
      body: FutureBuilder(
          future: getCloth(),
          builder: (context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            } else {
              return FocusDetector(
                  onFocusGained: trackScreen,
                  child: SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(25),
                      child: Column(children: [
                        Padding(padding: EdgeInsets.all(10)),
                        Text('${snapshot.data.title}', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),),
                        CachedNetworkImage(
                          imageUrl: '${snapshot.data.imagen}',
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error, color: Colors.white),
                        ),
                        Text(
                            '${snapshot.data.size} - ${snapshot.data.condition}', style: TextStyle(color: Colors.white)),
                        Text('${snapshot.data.price} COP', style: TextStyle(color: Colors.white)),
                        Text('${snapshot.data.description}', style: TextStyle(color: Colors.white)),
                        Text(
                            '${((snapshot.data.timestamp + snapshot.data.minutes * 60 - timestampActual) / 60).round()} minutos restantes', style: TextStyle(color: Colors.white)),
                        Container(
                          margin: const EdgeInsets.only(top: 8.0),
                          child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                      style: TextStyle(color: Colors.white),
                                      decoration: InputDecoration(
                                        labelText: 'Your bid',
                                        labelStyle: TextStyle(color: Colors.white),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: Colors.white),
                                        ),
                                      ),
                                      keyboardType: TextInputType.number,
                                      controller: bidController,
                                      validator: (String? value) {
                                        print(int.parse(value!) <
                                            snapshot.data.price);
                                        return (value != null &&
                                            int.parse(value) <=
                                                snapshot.data.price)
                                            ? 'Debes pujar un valor mayor al actual.'
                                            : null;
                                      }),
                                  Container(
                                    margin: const EdgeInsets.only(top: 8.0),
                                    child: CupertinoButton(
                                      color: Colors.white,
                                      onPressed: bidAuction,
                                      child: Text('Bid', style: TextStyle(color: Color.fromRGBO(16, 64, 59, 100))),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(top: 8.0),
                                    child: CupertinoButton(
                                      color: Colors.white,
                                      onPressed: addWishList,
                                      child: Text('Add to wishlist', style: TextStyle(color: Color.fromRGBO(16, 64, 59, 100))),
                                    ),
                                  )
                                ],
                              )
                          ),
                        )
                      ]),
                    ),
                  ));
            }
          }),
    );
  }
}
