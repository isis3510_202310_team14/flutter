import 'dart:convert';
import 'package:bidit/controller/GPS_controller.dart';
import 'package:bidit/controller/external_controller.dart';
import 'package:bidit/controller/image_controller.dart';
import 'package:bidit/controller/prenda_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart'; // Import the ConnectivityController
import 'package:bidit/controller/profile_controller.dart';
import 'package:bidit/controller/shared_controller.dart';
import 'package:bidit/model/prenda_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bidit/controller/analytics_controller.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateAuctionScreen extends StatefulWidget {
  const CreateAuctionScreen({Key? key}) : super(key: key);

  @override
  State<CreateAuctionScreen> createState() => _CreateAuctionScreenState();
}

class _CreateAuctionScreenState extends State<CreateAuctionScreen> {
  final _formKey = GlobalKey<FormState>();

  final gpsController = GPSController.getInstance();

  final AnalyticsController analyticsController = AnalyticsController();
  final ConnectivityController connectivityController =
      ConnectivityController(); // Create an instance of ConnectivityController

  void trackScreen() {
    analyticsController.trackScreen('createAuction', 'CreateAuctionScreen');
  }

  void initState() {
    loadPrice();
    getPrendasFromCache();
  }

  loadPrice() async {
    int precioAPI = await externalController.getPrices('shoes');
    setState(() {
      price = 'Precio recomendado: $precioAPI COP';
    });
  }

  var url = "";
  bool withImage = false;

  var price = "Precio recomendado: 0 COP";
  PrendaController prendaController = PrendaController();
  ExternalController externalController = ExternalController();
  TextEditingController titleController = TextEditingController();
  TextEditingController sizeController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController conditionController = TextEditingController();
  TextEditingController minutesController = TextEditingController();
  SharedController sharedController = SharedController();
  ImageController imageController = ImageController();

  onChangeDropDown(value) async {
    int precioAPI = await externalController.getPrices(value);
    setState(() {
      price = 'Precio recomendado: $precioAPI COP';
      dropdownValue = value;
    });
  }

  Future<void> storePrendaInCache(Prenda prenda) async {
    sharedController.set('title', prenda.title);
    sharedController.set('condition', prenda.condition);
    sharedController.set('size', prenda.size);
    sharedController.set('price', prenda.price);
    sharedController.set('minutes', prenda.minutes);
  }

  void getPrendasFromCache() async {
    bool isConnected = await connectivityController.isConnectedToInternet();

    if (!isConnected) {
      titleController.text = await sharedController.get('title');
      conditionController.text = await sharedController.get('condition');
      sizeController.text = await sharedController.get('size');
      priceController.text = await sharedController.get('price');
      minutesController.text = await sharedController.get('minutes');
    }

    if (await sharedController.get('title') != null && isConnected) {
      titleController.text = await sharedController.get('title');
    }
    if (await sharedController.get('condition') != null && isConnected) {
      conditionController.text = await sharedController.get('condition');
    }
    if (await sharedController.get('size') != null && isConnected) {
      sizeController.text = await sharedController.get('size');
    }
    if (await sharedController.get('price') != null && isConnected) {
      priceController.text = await sharedController.get('price');
    }
    if (await sharedController.get('minutes') != null && isConnected) {
      minutesController.text = await sharedController.get('minutes');
    }
  }

  void createAuction() async {
    bool isConnected = await connectivityController
        .isConnectedToInternet(); // Use isConnectedToInternet() method from ConnectivityController

    double latitude = 0;
    double longitude = 0;
    if (await gpsController.getPermissions() &&
        await gpsController.getStatusLocation()) {
      Position position = await gpsController.getPosition();
      latitude = position.latitude;
      longitude = position.longitude;
    }
    int timestamp = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    Prenda prenda = Prenda(
        description: dropdownValue,
        condition: conditionController.text,
        latitude: latitude,
        longitude: longitude,
        timestamp: timestamp,
        imagen: url,
        title: titleController.text,
        size: sizeController.text,
        price: int.parse(priceController.text),
        minutes: int.parse(minutesController.text));

    if (!isConnected) {
      await storePrendaInCache(prenda);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection'),
            content: Text('We draft your info, please try again later...'),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    } else {
      if(withImage==false){
        showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No image found'),
            content: Text('You need to choose an image'),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
      }else{
      prendaController.add(prenda);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("You create a bid successfully!"),
      ));
      }
      
    }
    conditionController.clear();
    _formKey.currentState?.reset();
  }

  String dropdownValue = "shoes";
  List list = ["shoes", "hoodie", "jean", "shirt", "tshirt"];

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
        onFocusGained: trackScreen,
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
          body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const Text('Create auction', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                      TextFormField(
                          decoration: const InputDecoration(labelText: 'Title', labelStyle: TextStyle(color: Colors.white)),
                          controller: titleController,
                          style: TextStyle(color: Colors.white)),
                      TextFormField(
                          decoration: const InputDecoration(labelText: 'Size', labelStyle: TextStyle(color: Colors.white)),
                          controller: sizeController,
                          style: TextStyle(color: Colors.white)),
                      TextFormField(
                          decoration: const InputDecoration(labelText: 'Price', labelStyle: TextStyle(color: Colors.white)),
                          controller: priceController,
                          keyboardType: TextInputType.number,
                          style: TextStyle(color: Colors.white)),
                      TextFormField(
                          decoration: const InputDecoration(labelText: 'Condition', labelStyle: TextStyle(color: Colors.white)),
                          controller: conditionController,
                          style: TextStyle(color: Colors.white)),
                      TextFormField(
                          decoration: const InputDecoration(labelText: 'Minutes', labelStyle: TextStyle(color: Colors.white)),
                          controller: minutesController,
                          keyboardType: TextInputType.number,
                          style: TextStyle(color: Colors.white)),
                      const Padding(padding: EdgeInsets.all(10)),
                      TextButton(
                          onPressed: () => onPressedUpload(context),
                          child: Text('Upload image',
                              style: TextStyle(
                                color: Color.fromRGBO(18, 115, 105, 1),
                              ))),
                      const Padding(padding: EdgeInsets.all(10)),
                      DropdownButton<String>(
                        dropdownColor: Colors.white,
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_downward, color: Colors.white),
                        elevation: 16,
                        underline: Container(
                          height: 2,
                          color: Colors.white,
                        ),
                        onChanged: onChangeDropDown,
                        items: <String>[
                          "shoes",
                          "hoodie",
                          "jean",
                          "shirt",
                          "tshirt"
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                      ),
                      Text('$price', style: TextStyle(color: Colors.white)),
                      const Padding(padding: EdgeInsets.all(10)),
                      CupertinoButton(
                        color: Colors.white,
                        onPressed: createAuction,
                        child: Text('Create Auction', style: TextStyle(color: Color.fromRGBO(18, 115, 105, 1))),
                      )
                    ],
                  ),
                )),
          ),
        ));
  }

  Future<void> onPressedUpload(BuildContext context) async {
    final ConnectivityController connectivityController =
        ConnectivityController(); // Create an instance of ConnectivityController
    bool isConnected = await connectivityController.isConnectedToInternet();
    if (!isConnected) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection'),
            content: Text('Please try again later...'),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("Image is being uploaded"),
      ));
      var imageUrl = await imageController.uploadImage();
      setState(() {
      url = imageUrl;
      withImage = true;
    });
    }
  }
}
