// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:bidit/controller/auth_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:bidit/model/user_model.dart';
import 'package:bidit/controller/analytics_controller.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String email = "";
  String password = "";
  // ignore: non_constant_identifier_names
  String? error_pass;
  // ignore: non_constant_identifier_names
  String? error_email;

  num errors = 0;

  // ignore: use_function_type_syntax_for_parameters

  AuthController authController = AuthController();

  final AnalyticsController analyticsController = AnalyticsController();

  void trackScreen() {
    analyticsController.trackScreen('register', 'RegisterScreen');
  }

  void register() async {
    print(error_pass);
    print("Register...");
    UserModel user = UserModel(
        ref: '',
        nombre: " ",
        apellido: " ",
        edad: 18,
        email: email,
        documento: 1,
        password: password);
    errors = await authController.register(user);
    if (errors == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("User created correctly, try log in"),
      ));
      Navigator.of(context).pop();
    } else if (errors == -1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("The password provided is too weak"),
      ));

      setState(() => error_pass = "The password provided is too weak");
    } else if (errors == -2) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("The account already exists for that email"),
      ));
      setState(() => error_email = "The account already exists for that email");
    } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("It's seems you don't have internet."),
        ));
        setState(() {
          error_email = 'No internet';
          error_pass = "No internet";
        });
      }
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        //Navigator.of(context).pushNamed("/");
      }
    });
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Column(children: [
          Container(
            alignment: Alignment.centerLeft,
            child: const BackButton(),
          ),
          const Text("Sign up",
              style: TextStyle(fontSize: 25, height: 3, color: Colors.black),
              textAlign: TextAlign.center),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              onChanged: (text) => validateEmail(text),
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
                enabledBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(
                      color: Color.fromRGBO(107, 78, 255, 1), width: 0.0),
                ),
                border: const OutlineInputBorder(),
                labelText: 'Email',
                errorText: error_email,
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 280,
            child: TextField(
              obscureText: true,
              onChanged: (text) => validatePassword(text),
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                  icon: Icon(Icons.lock_outline, color: Colors.deepPurple),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Color.fromRGBO(107, 78, 255, 1), width: 0.0),
                  ),
                  border: const OutlineInputBorder(),
                  errorText: error_pass,
                  labelText: 'Password'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
              width: 150,
              height: 40,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    //backgroundColor: const Color.fromRGBO(255, 87, 87, 1),
                    shape: RoundedRectangleBorder(
                        //to set border radius to button
                        borderRadius: BorderRadius.circular(30)),
                    backgroundColor: Color.fromRGBO(107, 78, 255, 1),
                  ),
                  onPressed: () {
                    onPressedRegister(context);
                  },
                  child: const Text('Sign up'))),
          const SizedBox(
            height: 20,
          ),
          Container(
            child: TextButton(
              onPressed: () {
                onPressedLogin(context);
              },
              child: const Text("Back to login",
                  style: TextStyle(
                    color: Color.fromRGBO(107, 78, 255, 1),
                  )),
            ),
          ),
        ])
      ]),
    ));
  }

  Future<void> onPressedRegister(BuildContext context) async {
    try {
      print(email);
      print(password);
      if (email == '' || password == '') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Try filling the inputs"),
        ));
      } else {
        register();
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("The password provided is too weak"),
        ));
      } else if (e.code == 'email-already-in-use') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("The account already exists for that email"),
        ));
        print('The account already exists for that email.');
      } else {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("It seems like you don't have internet"),
        ));
        setState(() {
          error_pass = "No internet";
          error_email = "No internet";
        });
      }
    } catch (e) {
      print(e);
    }
  }

  void validatePassword(String text) {
    setState(() {
      password = text;
    });

    if (text.length > 6) {
      setState(() {
        error_pass = null;
      });
    }
  }

  void validateEmail(String text) {
    setState(() {
      email = text;
    });

    if (RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(text)) {
      setState(() {
        error_email = null;
      });
    } else {
      setState(() {
        error_email = 'Must be a correct email';
      });
    }
  }

  void onPressedLogin(BuildContext context) {
    Navigator.of(context).pop();
  }
}
