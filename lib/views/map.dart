import 'dart:convert';

import 'package:bidit/controller/GPS_controller.dart';
import 'package:bidit/controller/analytics_controller.dart';
import 'package:bidit/controller/tienda_controller.dart';
import 'package:bidit/services/analytics_service.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:focus_detector/focus_detector.dart';
import 'dart:isolate';
import 'package:bidit/controller/prenda_controller.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({super.key});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late GoogleMapController mapController;
  final PrendaController prendaController = PrendaController();
  final gpsController = GPSController.getInstance();
  final TiendaController tiendaController = TiendaController();
  
  
  final AnalyticsController analyticsController = AnalyticsController();

  final LatLng _center = const LatLng(4.601943, -74.066632);

  final Set<Marker> _marker = {};

  Future loadMarkers() async {
    var list = await prendaController.getAll();
    var item;
    for (int i = 0; i<list.length; i++){
      item = list[i];
      _marker.add(Marker(
          markerId: MarkerId(item['id'].toString()),
          position: LatLng(item['latitude'], item['longitude']),
          infoWindow: InfoWindow(
              title: item['title'],
              snippet: "Condition: " + item['condition'])));
    }
    var listTiendas = await tiendaController.getAll();
    for (int i = 0; i<listTiendas.length; i++){
      item = listTiendas[i];
      _marker.add(Marker(
          markerId: MarkerId(item['id'].toString()),
          position: LatLng(item['latitude'], item['longitude']),
          infoWindow: InfoWindow(
              title: item['name']
          )));
    }
    return list;
  }

  void trackScreen(){
    analyticsController.trackScreen('map', 'MapScreen');
  }

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
  }

  void getShops() async {
    final resultPort = ReceivePort();
    if(await gpsController.getPermissions()){
      var location = await gpsController.getPosition();
      gpsController.getShopsNear();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      loadMarkers();
      getShops();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
        onFocusGained: trackScreen,
        child: FutureBuilder<dynamic>(
            future: loadMarkers(),
            builder: (context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                return Scaffold(
                    body: GoogleMap(
                        onMapCreated: _onMapCreated,
                        initialCameraPosition:
                            CameraPosition(target: _center, zoom: 16.0),
                        markers: _marker),
                        );
              } else {
                return const CircularProgressIndicator(
                  backgroundColor: Colors.white,
                  color: Colors.white,
                );
              }
            }));
  }
}
