import 'dart:async';
import 'dart:io';

import 'package:avatar_view/avatar_view.dart';
import 'package:bidit/controller/analytics_controller.dart';
import 'package:bidit/controller/auth_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/model/user_model.dart';
import 'package:bidit/views/wishlist.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:bidit/controller/profile_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File? file;

  UserModel? user;

  String errorText = '';

  String? imageUrl;

  RxBool isLoading = false.obs;

  ProfileController controller = ProfileController();
  AuthController authController = AuthController();

  final AnalyticsController analyticsController = AnalyticsController();

  void trackScreen() {
    analyticsController.trackScreen('profile', 'ProfileScreen');
  }

  void getUserData() async {
    UserModel response;
    try {
      response = await controller.getUserData();
    } catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection'),
            content:
                Text('Please check your internet connection and try again.'),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return;
    }

    _controllerName.text = response.nombre ?? '';
    if (response.email != null) {
      setState(() {
        imageUrl = response.email;
      });
    }
    setState(() {
      user = response;
    });
  }

  TextEditingController _controllerName = TextEditingController();

  void initState() {
    Get.testMode = true;
    super.initState();
    trackScreen();
    getUserData();
  }

  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        Navigator.of(context).pushNamed("/login");
      }
    });
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
        body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          const SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 5.0, right: 30.0, left: 30.0),
            child: AvatarView(
              radius: 60,
              avatarType: AvatarType.CIRCLE,
              imagePath: imageUrl ?? 'assets/images/default.png',
              placeHolder: Container(
                child: Icon(
                  Icons.person,
                  size: 120,
                ),
              ),
              errorWidget: Container(
                child: Icon(
                  Icons.error,
                  size: 120,
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () => onPressedUpload(context),
              child: Text('Change image',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ))),
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Expanded(
              flex: 5,
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 5.0, right: 40.0, left: 40.0),
                child: TextField(
                  controller: _controllerName,
                  style: TextStyle(color: Color.fromRGBO(211, 255, 251, 1)),
                  onChanged: (value) => {user?.setNombre(value)},
                  // ignore: prefer_const_constructors
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Name',
                    hintStyle: TextStyle(
                        color: Color.fromRGBO(211, 255, 251, 1)), //<-- SEE HERE
                    labelStyle:
                        TextStyle(color: Color.fromRGBO(211, 255, 251, 1)),
                    focusColor: Color.fromRGBO(139, 212, 205, 1),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(211, 255, 251, 1)),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(255, 255, 255, 1)),
                    ),
                    //errorText: error_email),
                  ),
                ),
              ),
            )
          ]),
          TextButton(
              onPressed: () => onPressedUpdate(context),
              child: Text("Save",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
          const SizedBox(height: 80),
          TextButton(
              onPressed: () => onGoToStore(context),
              child: Text(user?.store == null ? "Create store" : "Manage store",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
          const SizedBox(height: 20),
          TextButton(
              onPressed: () => onPressedAuctions(context),
              child: Text("My auctions",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
          TextButton(
              onPressed: () => onPressedWishlist(context),
              child: Text("My wishlist",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
          TextButton(
              onPressed: () => onPressed(context),
              child: Text("Settings",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
          const SizedBox(height: 10),
          TextButton(
              onPressed: () => onPressedLogOut(context),
              child: Text("Logout",
                  style: TextStyle(
                    color: Color.fromRGBO(251, 255, 251, 1),
                  ))),
        ]),
      ),
    );
  }

  Future<void> onPressedUpload(BuildContext context) async {
    try {
      var response = await controller.upload(user!);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("Image has been uploaded"),
      ));
      setState(() {
        imageUrl = response.email;
      });
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("There is no internet connection"),
      ));
    }
  }

  Future<void> onPressedUpdate(BuildContext context) async {
    try {
      var response = await controller.update(user!);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("Info is being updated"),
      ));

      setState(() {
        user = response;
      });
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("There is no internet connection"),
      ));
    }
  }

  Future<void> onPressedLogOut(BuildContext context) async {
    await FirebaseAuth.instance
        .signOut()
        .then((value) => Navigator.of(context).pushNamed("/login"));
  }

  Future<void> onPressed(BuildContext context) async {
    await Navigator.of(context).pushNamed("/settings");
  }

  Future<void> onGoToStore(BuildContext context) async {
    await Navigator.of(context).pushNamed("/store");
  }

  Future<void> onPressedAuctions(BuildContext context) async {
    await Navigator.of(context).pushNamed("/my-auctions");
  }

  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  Future<void> onPressedWishlist(BuildContext context) async {
    String? userId = await authController.getUserId();
    Navigator.push(context, MaterialPageRoute(builder: (context) => WishlistScreen(userId: userId ?? ''),),).then(onGoBack);
  }
}
