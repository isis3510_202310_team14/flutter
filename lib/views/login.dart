// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:bidit/controller/analytics_controller.dart';

import '../controller/auth_controller.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email = "";
  String password = "";
  // ignore: non_constant_identifier_names
  String? error_pass;
  // ignore: non_constant_identifier_names
  String? error_email;

  final AnalyticsController analyticsController = AnalyticsController();
  AuthController authController = AuthController();
  void trackScreen() {
    analyticsController.trackScreen('login', 'LoginScren');
  }

  void initState() {
    trackScreen();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {});
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Column(children: [
          Container(
            alignment: Alignment.centerLeft,
          ),
          const Text(
            "Log in",
            style: TextStyle(fontSize: 25, height: 3, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 300,
            child: TextField(
              onChanged: (value) => validateEmail(value),
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                  icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
                  border: const OutlineInputBorder(),
                  labelText: 'Email',
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Color.fromRGBO(107, 78, 255, 1), width: 0.0),
                  ),
                  errorText: error_email),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 300,
            child: TextField(
              obscureText: true,
              onChanged: (value) => validatePassword(value),
              // ignore: prefer_const_constructors
              decoration: InputDecoration(
                  icon: Icon(Icons.lock_outline, color: Colors.deepPurple),
                  border: const OutlineInputBorder(),
                  labelText: 'Password',
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Color.fromRGBO(107, 78, 255, 1), width: 0.0),
                  ),
                  errorText: error_pass),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
              width: 150,
              height: 40,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(120)),
                      backgroundColor: Color.fromRGBO(107, 78, 255, 1)),
                  onPressed: () {
                    onPressedLogin(context);
                  },
                  child: const Text('Log in'))),
          const SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: () {
              onPressedRegister(context);
            },
            child: const Text("Sign up",
                style: TextStyle(
                  color: Color.fromRGBO(107, 78, 255, 1),
                )),
          )
        ])
      ]),
    ));
  }

  Future<void> onPressedLogin(BuildContext context) async {
    try {
      if (email == '' || password == '') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Try filling the inputs"),
        ));
      } else {
        debugPrint(context.toString());
        final credential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password)
            .then((value) {
          Navigator.of(context).pushNamed("/nav");
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: const Text("Login succesful"),
          ));
        });
      }
      // ignore: use_build_context_synchronously
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // ignore: prefer_const_constructors
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("No user found for that email"),
        ));
        setState(() {
          error_email = 'No user found for that email';
        });
      } else if (e.code == 'wrong-password') {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Wrong password provided for that user"),
        ));
        setState(() {
          error_pass = "Wrong password provided for that user";
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("It seems you dont have internet"),
        ));
        setState(() {
          error_pass = "No internet";
          error_email = "No internet";
        });
      }
    }
  }

  void onPressedRegister(BuildContext context) {
    Navigator.of(context).pushNamed("/register");
  }

  void validateEmail(String text) {
    setState(() {
      email = text;
    });

    if (RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(text)) {
      setState(() {
        error_email = null;
      });
    } else {
      setState(() {
        error_email = 'Must be a correct email';
      });
    }
  }

  void validatePassword(String text) {
    setState(() {
      password = text;
    });

    if (text.length > 6) {
      setState(() {
        error_pass = null;
      });
    }
  }
}
