import 'package:avatar_view/avatar_view.dart';
import 'package:bidit/controller/GPS_controller.dart';
import 'package:bidit/controller/profile_controller.dart';
import 'package:bidit/controller/tienda_controller.dart';
import 'package:bidit/model/tienda_model.dart';
import 'package:bidit/model/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:bidit/controller/analytics_controller.dart';
import 'package:geolocator/geolocator.dart';

class ProfileStoreScreen extends StatefulWidget {
  const ProfileStoreScreen({Key? key}) : super(key: key);

  @override
  State<ProfileStoreScreen> createState() => _ProfileStoreScreenState();
}

class _ProfileStoreScreenState extends State<ProfileStoreScreen> {
  String? imageUrl;

  UserModel? user;
  Tienda? store;

  String? error_lat;
  // ignore: non_constant_identifier_names
  String? error_lon;

  final gpsController = GPSController.getInstance();

  TextEditingController _controllerLatitud = TextEditingController();
  TextEditingController _controllerLongitud = TextEditingController();
  TextEditingController _controllerName = TextEditingController();

  TiendaController _controller_store = new TiendaController();
  ProfileController _controller_user = ProfileController();
  final AnalyticsController analyticsController = AnalyticsController();
  void trackScreen() {
    analyticsController.trackScreen('profile', 'ProfileScreen');
  }

  void getUserData() async {
    UserModel response_user;
    Tienda response_store;
    try {
      response_user = await _controller_user.getUserData();
      response_store = await _controller_store.getStoreForUser(response_user);
    } catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Internet Connection'),
            content:
                Text('Please check your internet connection and try again.'),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return;
    }
    if (response_store.avatar != null) {
      imageUrl = response_store.avatar;
    }

    if (response_store != null && response_store.latitude != null) {
      _controllerLatitud.text = response_store.latitude.toString();
    }
    if (response_store != null && response_store.longitude != null) {
      _controllerLongitud.text = response_store.longitude.toString();
    }
    if (response_store != null && response_store.name != null) {
      _controllerName.text = response_store.name.toString();
    }
    setState(() {
      user = response_user;
      store = response_store;
    });
    print(imageUrl);
  }

  @override
  void initState() {
    super.initState();
    trackScreen();
    getUserData();
  }

  Widget build(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        // Navigator.of(context).pushNamed("/login");
      }
    });
    return SafeArea(
        child: Scaffold(
            backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
            body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(height: 40),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, right: 30.0, left: 30.0),
                    child: AvatarView(
                      radius: 60,
                      avatarType: AvatarType.CIRCLE,
                      imagePath: imageUrl ?? 'assets/images/default.png',
                      placeHolder: Container(
                        child: Icon(
                          Icons.person,
                          size: 120,
                        ),
                      ),
                      errorWidget: Container(
                        child: Icon(
                          Icons.error,
                          size: 120,
                        ),
                      ),
                    ),
                  ),
                  TextButton(
                      onPressed: () => onPressedUpload(context),
                      child: Text('Select image',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ))),
                  const SizedBox(height: 20),
                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5.0, right: 30.0, left: 30.0),
                        child: TextField(
                          controller: _controllerName,
                          style: TextStyle(
                              color: Color.fromRGBO(211, 255, 251, 1)),
                          onChanged: (value) => {store?.setName(value)},
                          // ignore: prefer_const_constructors
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: 'Name',
                            hintStyle: TextStyle(
                                color: Color.fromRGBO(
                                    211, 255, 251, 1)), //<-- SEE HERE
                            labelStyle: TextStyle(
                                color: Color.fromRGBO(211, 255, 251, 1)),
                            focusColor: Color.fromRGBO(139, 212, 205, 1),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(211, 255, 251, 1)),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                            //errorText: error_email),
                          ),
                        ),
                      ),
                    )
                  ]),
                  const SizedBox(height: 20),
                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5.0, right: 10.0, left: 30.0),
                        child: TextField(
                          controller: _controllerLatitud,
                          style: TextStyle(
                              color: Color.fromRGBO(211, 255, 251, 1)),
                          onChanged: (value) => {setLatitude(value)},
                          // ignore: prefer_const_constructors
                          decoration: InputDecoration(
                              border: const OutlineInputBorder(),
                              labelText: 'Latitude',
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(
                                      211, 255, 251, 1)), //<-- SEE HERE
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(211, 255, 251, 1)),
                              focusColor: Color.fromRGBO(139, 212, 205, 1),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color.fromRGBO(211, 255, 251, 1)),
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color.fromRGBO(255, 255, 255, 1)),
                              ),
                              errorText: error_lat
                              //errorText: error_email),
                              ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5.0, right: 30.0, left: 10.0),
                        child: TextField(
                          controller: _controllerLongitud,
                          onChanged: (value) => {setLongitude(value)},
                          style: TextStyle(
                              color: Color.fromRGBO(211, 255, 251, 1)),
                          // ignore: prefer_const_constructors
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: 'Longitude',
                            hintStyle: TextStyle(
                                color: Color.fromRGBO(
                                    211, 255, 251, 1)), //<-- SEE HERE
                            labelStyle: TextStyle(
                                color: Color.fromRGBO(211, 255, 251, 1)),
                            focusColor: Color.fromRGBO(139, 212, 205, 1),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(211, 255, 251, 1)),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                            //errorText: error_email),
                          ),
                        ),
                      ),
                    ),
                  ]),
                  TextButton(
                      onPressed: () => onPressedUseCurrent(context),
                      child: Text('Use current location',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ))),
                  const SizedBox(height: 80),
                  TextButton(
                      onPressed: () => onPressedUpdate(context),
                      child: Text('Save',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ))),
                  const SizedBox(height: 80),
                ])));
  }

  setLongitude(String value) {
    final n = double.tryParse(value);
    print(n);
    if (n == null) {
      setState(() {
        error_lon = 'Must be a number';
      });
    } else {
      setState(() {
        error_lon = null;
      });
      store?.setLongitude(n);
    }
  }

  setLatitude(String value) {
    final n = double.tryParse(value);
    print(n);
    if (n == null) {
      setState(() {
        error_lat = 'Must be a number';
      });
    } else {
      setState(() {
        error_lat = null;
      });
      store?.setLatitude(n);
    }
  }

  Future<void> onPressedUseCurrent(BuildContext context) async {
    try {
      if (await gpsController.getPermissions() &&
          await gpsController.getStatusLocation()) {
        Position position = await gpsController.getPosition();
        _controllerLatitud.text = position.latitude.toString();
        _controllerLongitud.text = position.longitude.toString();

        if (store != null) {
          store?.setLatitude(position.latitude);
          store?.setLongitude(position.longitude);
          setState(() {
            store:
            store;
          });
        }
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("There is no internet connection"),
      ));
    }
  }

  Future<void> onPressedUpload(BuildContext context) async {
    try {
      var response = await _controller_store.upload(store!);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("Image has been uploaded"),
      ));
      setState(() {
        imageUrl = response.avatar;
      });
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("There is no internet connection"),
      ));
    }
  }

  Future<void> onPressedUpdate(BuildContext context) async {
    try {
      if (store != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Info is being updated"),
        ));
        var response = await _controller_store.update(store!);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Info has been updated"),
        ));

        setState(() {
          store = response;
        });
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text("There is no internet connection"),
      ));
    }
  }
}
