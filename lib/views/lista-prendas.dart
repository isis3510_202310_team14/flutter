import 'dart:async';
import 'dart:convert';
import 'package:bidit/controller/GPS_controller.dart';
import 'package:bidit/controller/auth_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/controller/notification_controller.dart';
import 'package:bidit/controller/prenda_controller.dart';
import 'package:bidit/views/auction-profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bidit/controller/analytics_controller.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ListaPrendasScreen extends StatefulWidget {
  const ListaPrendasScreen({Key? key}) : super(key: key);

  @override
  State<ListaPrendasScreen> createState() => _ListaPrendasScreenState();
}

class _ListaPrendasScreenState extends State<ListaPrendasScreen> {
  void onPressedAuction(BuildContext context) async {
    final valueFromScreenB = await Navigator.of(context).pushNamed(
      '/auction',
    );
  }

  final gpsController = GPSController.getInstance();

  final AnalyticsController analyticsController = AnalyticsController();

  void trackScreen() {
    analyticsController.trackScreen('listClothes', 'ListaPrendasScreen');
  }

  void initState() {}

  PrendaController prendaController = PrendaController();
  NotificationController notificationController = NotificationController();
  AuthController authController = AuthController();
  ConnectivityController connectivityController = ConnectivityController();
  Color colorEstrella = Colors.black;

  Future<List> getClothes() async {
    final userId = await authController.getUserId();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isConnected = await connectivityController.isConnectedToInternet();
    List<Map<String, dynamic>> prendas =
        []; // Initialize prendas with an empty list

    if (!isConnected) {
      String? cachedData = prefs.getString('prendas');

      if (cachedData != null) {
        prendas = List<Map<String, dynamic>>.from(jsonDecode(cachedData));
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('No Internet Connection'),
              content:
                  Text('Please check your internet connection and try again.'),
              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } else {
      List preferencias = await prendaController.getAllByUser(userId);
      Map<String, dynamic> keys = {};
      var element;
      for (int i = 0; i < preferencias.length; i++) {
        element = preferencias[i];
        if (keys.containsKey(element['description']) &&
            keys[element['description']] != null) {
          keys[element['description']] = keys[element['description']] + 1;
        } else {
          keys[element['description']] = 1;
        }
      }

      prendas = (await prendaController.getAllSorted(keys))
          .map((item) => item as Map<String, dynamic>)
          .toList();
      prefs.setString('prendas', jsonEncode(prendas));
    }

    return prendas; // return the prendas variable
  }

  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: getClothes(),
        builder: (context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: CupertinoActivityIndicator());
          } else {
            return FocusDetector(
              onFocusGained: trackScreen,
              child: SafeArea(
                  child: Center(
                      child: Column(
                children: [
                  const Padding(padding: EdgeInsets.all(50)),
                  Text("BIDIT",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Monserrat',
                          fontSize: 45,
                          color: Color.fromRGBO(138, 166, 163, 100))),
                  const Padding(padding: EdgeInsets.all(50)),
                  AspectRatio(
                      aspectRatio: 9 / 7,
                      child: SizedBox(
                          child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            color: Color.fromRGBO(191, 191, 191, 100),
                            margin: EdgeInsets.all(15),
                            child: Container(
                                constraints: const BoxConstraints(
                                    minHeight: 0, maxHeight: 100.0),
                                child: Column(
                                  children: [
                                    Padding(padding: EdgeInsets.only(top: 10)),
                                    Row(
                                      children: [
                                        Text(
                                          '${snapshot.data[index]['title']}',
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Monserrat'),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                        child: SizedBox(
                                            height: 200,
                                            child: CachedNetworkImage(
                                              fit: BoxFit.fitWidth,
                                              imageUrl:
                                                  '${snapshot.data[index]['imagen']}',
                                              width: 350,
                                              placeholder: (context, url) =>
                                                  CupertinoActivityIndicator(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Icon(Icons.error),
                                            )),
                                        onTap: () {
                                          var prenda =
                                              snapshot.data[index]["ref"];
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  ProfileAuctionScreen(
                                                      prendaId: prenda),
                                            ),
                                          ).then(onGoBack);
                                        },
                                        onVerticalDragUpdate: (details) {
                                          int sensitivity = 44;
                                          if (details.delta.dy > sensitivity) {
                                            setState(() {
                                              colorEstrella = Colors.black;
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: const Text(
                                                    "Removed from your wishlist"),
                                              ));
                                            });
                                          } else if (details.delta.dy <
                                              -sensitivity) {
                                            setState(() {
                                              colorEstrella = Color.fromRGBO(
                                                  237, 198, 0, 1);
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: const Text(
                                                    "Added to your wishlist"),
                                              ));
                                            });
                                          }
                                        }),
                                    Text(
                                        '${snapshot.data[index]['size']} - ${snapshot.data[index]['condition']}',
                                        style: const TextStyle(
                                            fontFamily: 'Monserrat')),
                                    Text('${snapshot.data[index]['price']} COP',
                                        style: const TextStyle(
                                            fontFamily: 'Monserrat')),
                                  ],
                                )),
                          );
                        },
                      )))
                ],
              ))),
            );
          }
        },
      ),
    );
  }
}
