import 'package:bidit/controller/analytics_controller.dart';
import 'package:bidit/controller/auth_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/controller/prenda_controller.dart';
import 'package:bidit/controller/tienda_controller.dart';
import 'package:bidit/controller/total_controller.dart';
import 'package:bidit/controller/wishlist_controller.dart';
import 'package:bidit/model/prenda_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class WishlistScreen extends StatefulWidget {
  @override
  _WishlistScreenState createState() => _WishlistScreenState(userId: userId);

  const WishlistScreen({Key? key, required this.userId}) : super(key: key);

  final String userId;
}

class _WishlistScreenState extends State<WishlistScreen> {
  _WishlistScreenState({required this.userId});

  final String userId;

  AuthController authController = AuthController();
  ConnectivityController connectivityController = ConnectivityController();
  WishlistController wishlistController = WishlistController();
  PrendaController prendaController = PrendaController();

  List ids = [];

  void remove(refId) async {
    bool isConnected = await connectivityController.isConnectedToInternet();
    if(!isConnected){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: const Text("You don't have internet connection, try again later."),
    ));
    }else{
wishlistController.delete(refId);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: const Text("Item removed from your wishlist"),
    ));
    }
    
  }

  Future<List> getClothes() async {
    final userId = await authController.getUserId();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isConnected = await connectivityController.isConnectedToInternet();
    List ret = [];

    List<Map<String, dynamic>> prendas =
        []; // Initialize prendas with an empty list

    if (!isConnected) {
      String? cachedData = prefs.getString('wish');

      if (cachedData != null) {
        prendas = List<Map<String, dynamic>>.from(jsonDecode(cachedData));
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('No Internet Connection'),
              content:
                  Text('Please check your internet connection and try again.'),
              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } else {
      prendas = (await wishlistController.getAllByUser(userId))
          .map((item) => item as Map<String, dynamic>)
          .toList();
      prefs.setString('wish', jsonEncode(prendas));

      var element;
      for (int i = 0; i < prendas.length; i++) {
        var element = prendas[i];
        print('******');
        String clothesRef = element['clothesRef'];
        Prenda aux = await prendaController.getOne(clothesRef);
        ret.add(aux);
        ids.add(element['ref']);
      }
    }
    return ret; // return the prendas variable
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: getClothes(),
        builder: (context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: CupertinoActivityIndicator());
          } else {
            return FocusDetector(
              child: SafeArea(
                  child: Center(
                      child: Column(
                children: [
                  const Padding(padding: EdgeInsets.all(50)),
                  Text("My wishlist",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Monserrat',
                          fontSize: 45,
                          color: Color.fromRGBO(138, 166, 163, 100))),
                  const Padding(padding: EdgeInsets.all(50)),
                  AspectRatio(
                      aspectRatio: 9 / 7,
                      child: SizedBox(
                          child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            margin: EdgeInsets.all(10),
                            color: Color.fromRGBO(191, 191, 191, 100),
                            child: Container(
                                constraints: const BoxConstraints(
                                    minHeight: 50),
                                child: Row(
                                  children: [
                                    Padding(
                                        padding:
                                            EdgeInsets.only(top: 10, left: 25)),
                                    SizedBox(
                                      height: 150,
                                      width: 150,
                                      child: CachedNetworkImage(
                                        fit: BoxFit.contain,
                                        imageUrl:
                                            '${snapshot.data[index].imagen}',
                                        placeholder: (context, url) =>
                                            CupertinoActivityIndicator(),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 20)),
                                    Column(
                                      children: [
                                        Text(
                                            '${snapshot.data[index].title}',
                                            style: const TextStyle(
                                                fontFamily: 'Monserrat',
                                                fontWeight: FontWeight.bold)),
                                          Text(
                                            '${snapshot.data[index].condition}',
                                            style: const TextStyle(
                                                fontFamily: 'Monserrat',
                                                fontWeight: FontWeight.bold)),
                                          Text(
                                            '${snapshot.data[index].size}',
                                            style: const TextStyle(
                                                fontFamily: 'Monserrat',
                                                fontWeight: FontWeight.bold)),
                                          Text(
                                            '${snapshot.data[index].price} COP',
                                            style: const TextStyle(
                                                fontFamily: 'Monserrat',
                                                fontWeight: FontWeight.bold))
                                      ],
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 20)),
                                    GestureDetector(
                                      child: Icon(Icons.delete_forever, size: 50,),
                                      onTap: () {
                                        remove(index);
                                      },
                                    )
                                    
                                  ],
                                )),
                          );
                        },
                      )))
                ],
              ))),
            );
          }
        },
      ),
    );
  }
}
