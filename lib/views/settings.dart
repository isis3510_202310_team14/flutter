import 'dart:io';
import 'package:bidit/controller/settings_controller.dart';
import 'package:flutter/material.dart';

class SettingsScrenn extends StatefulWidget {
  const SettingsScrenn({Key? key}) : super(key: key);

  @override
  State<SettingsScrenn> createState() => _SettingsScrennState();
}

class _SettingsScrennState extends State<SettingsScrenn> {
  double radiusP = 0.0;
  TextEditingController radioController = TextEditingController();
  SettingController settingController = SettingController();

  void onPressedSave() {
    String radius = radioController.text;
    settingController.setRadius(double.parse(radius));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: const Text("Your settings has been saved!"),
    ));
  }

  void initState() {
    getRadiusActual();
  }

  void getRadiusActual() async {
    radiusP = await settingController.getRadius();
    radioController.text = radiusP.toString();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(16, 64, 59, 100),
      body: Column(
        children: [
          const Padding(padding: EdgeInsets.all(15)),
          const Text("General settings",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          const Padding(padding: EdgeInsets.all(25)),
          const Text("Store search radius (400 m by default)"),
          Padding(
            padding: const EdgeInsets.only(left: 35, right: 35, bottom: 15),
            child: Form(
                child: TextFormField(
              controller: radioController,
              decoration: const InputDecoration(labelText: 'Radius (m)'),
              keyboardType: TextInputType.number,
            )),
          ),
          ElevatedButton(onPressed: () {onPressedSave();}, child: const Text("Save"))
        ],
      ),
    ));
  }
}

