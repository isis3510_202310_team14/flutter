// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prenda_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Prenda _$PrendaFromJson(Map<String, dynamic> json) => Prenda(
      ref: json['ref'] as String?,
      userRef: json['userRef'] as String?,
      description: json['description'] as String?,
      condition: json['condition'] as String?,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      timestamp: json['timestamp'] as int,
      imagen: json['imagen'] as String?,
      title: json['title'] as String?,
      size: json['size'] as String?,
      price: json['price'] as int?,
      minutes: json['minutes'] as int,
    );

Map<String, dynamic> _$PrendaToJson(Prenda instance) => <String, dynamic>{
      'ref': instance.ref,
      'title': instance.title,
      'size': instance.size,
      'price': instance.price,
      'description': instance.description,
      'condition': instance.condition,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'timestamp': instance.timestamp,
      'imagen': instance.imagen,
      'minutes': instance.minutes,
      'userRef': instance.userRef,
    };
