import 'package:json_annotation/json_annotation.dart';

part 'tienda_model.g.dart';

@JsonSerializable()
class Tienda {
  String? ref;
  double? latitude;
  double? longitude;
  String? name;
  String? avatar;
  List<String>? prendas;

  Tienda(
      {this.ref,
      required this.latitude,
      required this.longitude,
      required this.name,
      this.avatar,
      this.prendas});

  String? get getRef => this.ref;

  set setRef(String? ref) => this.ref = ref;

  get getLatitude => this.latitude;

  setLatitude(latitude) => this.latitude = latitude;

  get getAvatar => this.avatar;

  setAvatar(avatar) => this.avatar = avatar;

  get getLongitude => this.longitude;

  setLongitude(longitude) => this.longitude = longitude;

  get getName => this.name;

  setName(name) => this.name = name;

  get getPrendas => this.prendas;

  set setPrendas(prendas) => this.prendas = prendas;

  factory Tienda.fromJson(Map<String, dynamic> json) => _$TiendaFromJson(json);
  Map<String, dynamic> toJson() => _$TiendaToJson(this);
}
