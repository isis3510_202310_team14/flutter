import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';

part 'prenda_model.g.dart';

@JsonSerializable()
class Prenda {
  String? ref;
  String? title;
  String? size;
  int? price;
  String? description;
  String? condition;
  double latitude;
  double longitude;
  int timestamp;
  String? imagen;
  int minutes;
  String? userRef;

  Prenda(
      {
      this.ref,
      this.userRef,
      required this.description,
      required this.condition,
      required this.latitude,
      required this.longitude,
      required this.timestamp,
      required this.imagen,
      required this.title,
      required this.size,
      required this.price,
      required this.minutes});

  factory Prenda.fromJson(Map<String, dynamic> json) => _$PrendaFromJson(json);
  Map<String, dynamic> toJson() => _$PrendaToJson(this);

  String? get getRef => this.ref;

  set setRef(String? ref) => this.ref = ref;

  get getTitle => this.title;

  set setTitle(title) => this.title = title;

  get getSize => this.size;

  set setSize(size) => this.size = size;

  get getPrice => this.price;

  get getDescription => this.description;

  set setDescription(description) => this.description = description;

  get getCondition => this.condition;

  set setCondition(condition) => this.condition = condition;

  get getLatitude => this.latitude;

  set setLatitude(latitude) => this.latitude = latitude;

  get getLongitude => this.longitude;

  set setLongitude(longitude) => this.longitude = longitude;

  get getTimestamp => this.timestamp;

  set setTimestamp(timestamp) => this.timestamp = timestamp;

  get getImagen => this.imagen;

  set setImagen(imagen) => this.imagen = imagen;

  get getMinutes => this.minutes;

  set setMinutes(minutes) => this.minutes = minutes;

  void setPrice(price) {
    this.price = price;
  }

  void setUserRef(userRef) {
    this.userRef = userRef;
  }
}
