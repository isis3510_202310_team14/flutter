import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  String ref;
  String? nombre;
  String? apellido;
  String? email;
  String? password;
  String? imagen;
  String? store;

  int? edad;
  int? documento;

  UserModel(
      {required this.ref,
      this.password,
      this.imagen,
      this.store,
      required this.nombre,
      required this.apellido,
      required this.edad,
      required this.email,
      required this.documento});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  String? get getRef => this.ref;

  setRef(String ref) => this.ref = ref;

  get getNombre => this.nombre;

  setNombre(nombre) => this.nombre = nombre;

  get getStore => this.store;

  setStore(store) => this.store = store;

  get getApellido => this.apellido;

  setApellido(apellido) => this.apellido = apellido;

  String? get getPassword => this.password;

  setPassword(String? password) => this.password = password;

  get getEdad => this.edad;

  setEdad(edad) => this.edad = edad;

  get getDocumento => this.documento;

  setDocumento(documento) => this.documento = documento;

  get getEmail => this.email;

  setEmail(email) => this.email = email;

  get getImagen => this.imagen;

  setImagen(imagen) => this.imagen = imagen;
}
