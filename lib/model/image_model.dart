class ImageModel {
  String? link;
  String? error;
  int source = 1;
  bool hasError = false;
  bool hasLink = false;
  ImageModel();

  setLink(String? link) => this.link = link;
  get getLink => this.link;

  setError(String? error) => this.error = error;
  get getError => this.error;

  setHasError(bool hasError) => this.hasError = hasError;
  get getHasError => this.hasError;

  setHasLink(bool hasLink) => this.hasLink = hasLink;
  get getHasLink => this.hasLink;

  setSource(int source) => this.source = source;
  get getSource => this.source;
}
