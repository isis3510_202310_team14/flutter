// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      ref: json['ref'] as String,
      password: json['password'] as String?,
      imagen: json['imagen'] as String?,
      store: json['store'] as String?,
      nombre: json['nombre'] as String?,
      apellido: json['apellido'] as String?,
      edad: json['edad'] as int?,
      email: json['email'] as String?,
      documento: json['documento'] as int?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'ref': instance.ref,
      'nombre': instance.nombre,
      'apellido': instance.apellido,
      'email': instance.email,
      'password': instance.password,
      'imagen': instance.imagen,
      'store': instance.store,
      'edad': instance.edad,
      'documento': instance.documento,
    };
