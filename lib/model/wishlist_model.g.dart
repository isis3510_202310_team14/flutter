// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wishlist_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Wishlist _$WishlistFromJson(Map<String, dynamic> json) => Wishlist(
      ref: json['ref'] as String,
      userRef: json['userRef'] as String,
      clothesRef: json['clothesRef'] as String,
    );

Map<String, dynamic> _$WishlistToJson(Wishlist instance) => <String, dynamic>{
      'ref': instance.ref,
      'userRef': instance.userRef,
      'clothesRef': instance.clothesRef,
    };
