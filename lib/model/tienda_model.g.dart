// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tienda_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tienda _$TiendaFromJson(Map<String, dynamic> json) => Tienda(
      ref: json['ref'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
      prendas:
          (json['prendas'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$TiendaToJson(Tienda instance) => <String, dynamic>{
      'ref': instance.ref,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'name': instance.name,
      'avatar': instance.avatar,
      'prendas': instance.prendas,
    };
