import 'package:json_annotation/json_annotation.dart';

part "wishlist_model.g.dart";

@JsonSerializable()
class Wishlist {
  String? ref;
  String userRef;
  String clothesRef;

  Wishlist(
      {
      this.ref,
      required this.userRef,
      required this.clothesRef,});

  String get getUserRef => this.userRef;

  set setUserRef(String ref) => this.userRef = ref;

  String get getclothesRef=> this.clothesRef;

  set setclothesRef(String ref) => this.clothesRef = ref;

  factory Wishlist.fromJson(Map<String, dynamic> json) =>
      _$WishlistFromJson(json);

  Map<String, dynamic> toJson() => _$WishlistToJson(this);
}
