import 'package:bidit/model/user_model.dart';
import 'package:bidit/services/image_service.dart';
import 'package:permission_handler/permission_handler.dart';

class ImageController {

  ImageClothesService imageService = ImageClothesService();

  Future<String> uploadImage() async {
    return await imageService.upload();
  } 
  
}
