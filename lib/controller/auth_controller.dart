import 'package:bidit/model/user_model.dart';
import 'package:bidit/services/auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthController {
  AuthService authService = AuthService();

  Future<num> register(user) async {
    try {
      var response = await authService.register(user);
      return response ? 1 : 0;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        // ignore: prefer_const_constructors
        return -1;
      } else if (e.code == 'email-already-in-use') {
        // ignore: prefer_const_constructors
        print('The account already exists for that email.');
        return -2;
      }
    } catch (e) {
      return -10;
    }
    return -10;
  }

  Future<num> login(username, password) async {
    try {
      await authService.login(username, password);
      // ignore: use_build_context_synchronously
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // ignore: prefer_const_constructors
        return -1;
      } else if (e.code == 'wrong-password') {
        // ignore: prefer_const_constructors
        return -2;
      } else {
        print(e.code);
        // ignore: prefer_const_constructors
        return -2;
      }
    }
    return 1;
  }

  Future<String?> getUserId() async {
    return await authService.getUserId();
  }
}
