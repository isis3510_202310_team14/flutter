import 'dart:convert';

import 'package:bidit/controller/auth_controller.dart';
import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/model/user_model.dart';
import 'package:bidit/repositories/user_repository.dart';
import 'package:bidit/services/image_upload_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/image_model.dart';

class ProfileController {
  ImageService service = ImageService();
  UserRepository repository = UserRepository();
  ConnectivityController connectivityController = ConnectivityController();

  AuthController authController = AuthController();

  Future<UserModel> getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userId = await authController.getUserId();
    var response;
    bool isConnected = await connectivityController.isConnectedToInternet();
    String? cachedData = prefs.getString('user');

    if (cachedData != null) {
      response = UserModel.fromJson(jsonDecode(cachedData));
      if (response.ref != userId) {
        if (isConnected) {
          response = await repository.getById(userId);
          prefs.setString('user', jsonEncode(response));
        } else {
          throw Exception(['nc', 'not connected to internet']);
        }
      } else {
        if (isConnected) {
          response = await repository.getById(userId);
          prefs.setString('user', jsonEncode(response));
        } else if (cachedData != null) {
          response = UserModel.fromJson(jsonDecode(cachedData));
        } else {
          throw Exception(['nc', 'not connected to internet']);
        }
      }
    } else {
      if (isConnected) {
        response = await repository.getById(userId);
        prefs.setString('user', jsonEncode(response));
      } else {
        throw Exception(['nc', 'not connected to internet']);
      }
    }
    return response;
  }

  Future<UserModel> upload(UserModel user) async {
    var response = new ImageModel();
    bool isConnected = await connectivityController.isConnectedToInternet();
    if (isConnected) {
      response = await service.upload(response, user);
      if (response.hasLink) {
        user.setEmail(response.getLink);
        repository.update(user.ref, user);
      }
    } else {
      throw Exception(['nc', 'not connected to internet']);
    }
    return user;
  }

  Future<UserModel> update(UserModel user) async {
    bool isConnected = await connectivityController.isConnectedToInternet();
    if (isConnected) {
      repository.update(user.ref, user);
      getUserData();
    } else {
      throw Exception(['nc', 'not connected to internet']);
    }
    return user;
  }
}
