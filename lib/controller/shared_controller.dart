import 'package:bidit/services/shared_service.dart';

class SharedController {
  SharedService sharedService = SharedService();

  void set(key, value){
    if(value.runtimeType == bool){
      sharedService.setBool(key, value);
    } else if (value.runtimeType == double){
      sharedService.setDouble(key, value);
    } else if (value.runtimeType == int){
      sharedService.setInt(key, value);
    } else if (value.runtimeType == String){
      sharedService.setString(key, value);
    } else if (value.runtimeType == List<String>){
      sharedService.setStringList(key, value);
    }
  }

  dynamic get(key) {
    return sharedService.get(key);
  }
  void removeCache(String key) {
    sharedService.remove(key);
  }
}
