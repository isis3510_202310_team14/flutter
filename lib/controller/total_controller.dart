import 'package:bidit/repositories/total_repository.dart';

class TotalController {
  TotalRepository totalRepository = TotalRepository();
  
  Future<void> update() async {
    int total = 0;
    var data = await totalRepository.getAll();
    var element;
    for (int i=0;i<data.length; i++){
      element = data[i];
      total = element['total'];
    }
    total = total + 1;
    var aux = {
      'total': total
    };
    await totalRepository.updateOnly(aux);
  }
  
}