import 'package:bidit/repositories/prenda_repository.dart';
import 'package:bidit/model/wishlist_model.dart';
import 'package:bidit/repositories/wishlist_repository.dart';

class WishlistController {

  WishlistRepository wishlistRepository = WishlistRepository();

  Future<List> getAll() {
    return wishlistRepository.getAll();
  }

  Future<List> getAllByUser(userId) async {
    List data = await wishlistRepository.getAllByUser(userId);
    print(data);
    return data;
  }

  Future<Wishlist> getOne(String id) {
    Future<Wishlist> prenda = wishlistRepository.getOne(id);
    return prenda;
  }

  Future<String> add(Wishlist wish) async {
    String docId = await wishlistRepository.add(wish);
    wish.ref = docId;
    print(docId);
    update(docId, wish);
    return docId;
  }

  Future<void> update(String id, Wishlist prenda){
    return wishlistRepository.update(id, prenda);
  }

  Future<void> delete(String id){
    return wishlistRepository.delete(id);
  }
}