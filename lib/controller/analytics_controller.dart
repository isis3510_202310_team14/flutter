import 'package:bidit/services/analytics_service.dart';

class AnalyticsController {
  AnalyticsService analyticsService = AnalyticsService();
  void trackScreen(screenName, screenClass){
    analyticsService.trackScreen(screenName, screenClass);
  }
}