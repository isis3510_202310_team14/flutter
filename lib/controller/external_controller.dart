import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/services/external_service.dart';
import 'package:flutter/foundation.dart';

class ExternalController {
  ExternalService externalService = ExternalService();
  ConnectivityController connectivityController = ConnectivityController();

  Future<dynamic> getPrices(category) async {
    if (await connectivityController.isConnectedToInternet()) {
      print("Internet");
      Map prices = await externalService.fetchPrices();
      int valor = 0;
      var element;
      for (int i=0;i<prices["prices"].length;i++){
        element = prices["prices"][i];
        if (element['category'] == category) {
          valor = element['avg'];
        }
      }
      return valor;
    } else {
      return 0;
    }
  }
}
