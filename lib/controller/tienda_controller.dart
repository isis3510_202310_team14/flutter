import 'dart:convert';

import 'package:bidit/controller/connectivity_controller.dart';
import 'package:bidit/model/image_model.dart';
import 'package:bidit/model/user_model.dart';
import 'package:bidit/repositories/tienda_repository.dart';
import 'package:bidit/model/tienda_model.dart';
import 'package:bidit/services/image_upload_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TiendaController {
  TiendaRepository tiendaRepository = TiendaRepository();

  ImageService service = ImageService();

  ConnectivityController connectivityController = ConnectivityController();
  Future<List> getAll() {
    return tiendaRepository.getAll();
  }

  Future<Tienda?> getOne(String id) {
    return tiendaRepository.getOne(id);
  }

  Future<String> add(Tienda tienda) {
    return tiendaRepository.add(tienda);
  }

  Future<Tienda> update(Tienda tienda) async {
    bool isConnected = await connectivityController.isConnectedToInternet();
    if (isConnected) {
      var ref = tienda.ref ?? '';
      await tiendaRepository.update(ref, tienda);
    } else {
      throw Exception(['nc', 'not connected to internet']);
    }
    return tienda;
  }

  Future<void> delete(String id) {
    return tiendaRepository.delete(id);
  }

  Future<Tienda> getStoreForUser(UserModel user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var response;
    ImageService service = ImageService();

    bool isConnected = await connectivityController.isConnectedToInternet();
    String? cachedData = prefs.getString('store');

    if (cachedData != null) {
      response = Tienda.fromJson(jsonDecode(cachedData));
      if (response.ref != user.ref) {
        if (isConnected) {
          response = await tiendaRepository.getByUserId(user);
          prefs.setString('store', jsonEncode(response));
        } else {
          throw Exception(['nc', 'not connected to internet']);
        }
      } else {
        if (isConnected) {
          response = await tiendaRepository.getByUserId(user);
          prefs.setString('store', jsonEncode(response));
        } else if (cachedData != null) {
          response = Tienda.fromJson(jsonDecode(cachedData));
        } else {
          throw Exception(['nc', 'not connected to internet']);
        }
      }
    } else {
      if (isConnected) {
        response = await await tiendaRepository.getByUserId(user);
        prefs.setString('store', jsonEncode(response));
      } else {
        throw Exception(['nc', 'not connected to internet']);
      }
    }
    return response;
  }

  Future<Tienda> upload(Tienda tienda) async {
    var response = new ImageModel();
    bool isConnected = await connectivityController.isConnectedToInternet();
    if (isConnected) {
      response = await service.uploadTienda(response, tienda);
      if (response.hasLink) {
        tienda.setAvatar(response.getLink);
        if (tienda != null && tienda.ref != null) {
          var ref = tienda.ref;
          tiendaRepository.update(ref!, tienda);
        }
      }
    } else {
      throw Exception(['nc', 'not connected to internet']);
    }
    return tienda;
  }
}
