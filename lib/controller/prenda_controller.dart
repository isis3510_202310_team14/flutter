import 'package:bidit/repositories/prenda_repository.dart';
import 'package:bidit/model/prenda_model.dart';

class PrendaController {

  PrendaRepository prendaRepository = PrendaRepository();

  Future<List> getAll() {
    return prendaRepository.getAll();
  }

  Future<List> getAllSorted(Map<String, dynamic> preferencias) async {
    List prendas = await prendaRepository.getAll();
    for (int i = 0; i < prendas.length; i++) {
      if(preferencias.containsKey(prendas[i]['description'])){
        prendas[i]['weight'] = preferencias[prendas[i]['description']];
      } else {
        prendas[i]['weight'] = 0;
      }
    }
    prendas.sort( (a,b) {
      return b['weight'].compareTo(a['weight']);
    });
    return prendas;
  }

  Future<List> getAllByUser(userId) {
    return prendaRepository.getAllByUser(userId);
  }

  Future<Prenda> getOne(String id) {
    Future<Prenda> prenda = prendaRepository.getOne(id);
    return prenda;
  }

  Future<String> add(Prenda prenda) async {
    String docId = await prendaRepository.add(prenda);
    prenda.ref = docId;
    print(docId);
    update(docId, prenda);
    return docId;
  }

  Future<void> update(String id, Prenda prenda){
    return prendaRepository.update(id, prenda);
  }

  Future<void> delete(String id){
    return prendaRepository.delete(id);
  }
}