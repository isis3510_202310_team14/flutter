import 'package:bidit/services/notification_services.dart';

class NotificationController{
  NotificationService notificationService = NotificationService();

  void addNotification(String title, String body){
    notificationService.addNotification(title, body);
  }
}