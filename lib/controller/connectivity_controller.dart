import 'package:internet_connection_checker/internet_connection_checker.dart';

class ConnectivityController{
  Future<bool> isConnectedToInternet() async {
    bool result = await InternetConnectionChecker().hasConnection;
    return result;
  }
}