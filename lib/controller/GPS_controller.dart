import 'package:bidit/controller/notification_controller.dart';
import 'package:bidit/controller/settings_controller.dart';
import 'package:bidit/controller/tienda_controller.dart';
import 'package:geolocator/geolocator.dart';
import 'package:bidit/utils/utils_location.dart';
import 'dart:isolate';
import 'dart:math' as math;


class GPSController {

  GPSController._internal();

  static final GPSController _instance = GPSController._internal();

  TiendaController tiendaController = TiendaController();
  NotificationController notificationController = NotificationController();
  SettingController settingController = SettingController();

  Future<bool> getStatusLocation() async {
    return await Geolocator.isLocationServiceEnabled();
  }

  factory GPSController(){
    return _instance;
  }

  static GPSController getInstance(){
    return _instance;
  }

  Future<bool> getPermissions() async {

    LocationPermission permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
            return false;
        }else if(permission == LocationPermission.deniedForever){
            return true;
        }else{
            return true;
        }
    }else{
        return true;
    }
  }

  void getDistances(Map map) async {
    final SendPort port = map['port'];
    final List data = map['data'];
    final double latitude = map['latitude'];
    final double longitude = map['longitude'];

    List distances = [];

    double earthRadius = 6371;

    var element;

    for (int i = 0; i<data.length; i++){

      element = data[i];

      double dLat = (element['latitude']-latitude) * math.pi / 180;
      double dLon = (element['longitude']-longitude) * math.pi / 180;

      double lat1 = latitude * math.pi / 180;
      double lat2 = element['latitude'] * math.pi / 180;

      var a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(lat1) * math.cos(lat2); 
      var c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a)); 
      
      double distance = earthRadius * c;
      var aux = {'name': element['name'], 'distance': distance};
      distances.add(aux);
    }

    distances.sort((a, b) {
      return a['distance'].compareTo(b['distance']);
    });
    port.send(distances);
  }

  void getShopsNear() async {
    List shops = await tiendaController.getAll();
    var location = await getPosition();
    double latitude = location.latitude;
    double longitude = location.longitude;

    final p = ReceivePort();
    final data = {'port': p.sendPort, 'data' : shops, 'latitude': latitude, 'longitude': longitude};
    final isolate = await Isolate.spawn(getDistances, data);

    final distances = await p.first;

    double distanceBase = await settingController.getRadius() / 1000;
    if (distances[0]['distance'] < distanceBase) {
      String nombre = distances[0]['name'];
      String body = 'La tienda $nombre está muy cerca de ti, visitala';
      notificationController.addNotification(
          '¡Tienes una tienda cercana!', body);
    }
  }

  Future<Position> getPosition() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return position;
  }

}
