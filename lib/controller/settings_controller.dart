import 'package:bidit/controller/shared_controller.dart';

class SettingController {
  SharedController sharedController = SharedController();

  Future<double> getRadius() async {
    if(await sharedController.get('radius') == null){
      return 400;
    }
    return await sharedController.get('radius');
  }

  void setRadius(value){
    sharedController.set('radius', value);
  }
}
