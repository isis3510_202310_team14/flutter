import 'package:bidit/model/tienda_model.dart';
import 'package:bidit/model/user_model.dart';
import 'package:bidit/repositories/IRepositoryFirebase.dart';
import 'package:bidit/repositories/user_repository.dart';
import 'package:bidit/services/firestore_service.dart';

class TiendaRepository implements IRepository<Tienda> {
  var shop_collection = 'shop';

  UserRepository _user_repository = new UserRepository();

  FirebaseConnection firebaseConnection = FirebaseConnection();

  @override
  Future<String> add(Tienda t) async {
    return await firebaseConnection.addToCollection(
        shop_collection, t.toJson());
  }

  @override
  Future<void> delete(String id) async {
    return await firebaseConnection.delete(shop_collection, id);
  }

  @override
  Future<List> getAll() async {
    return await firebaseConnection.getCollection(shop_collection);
  }

  @override
  Future<Tienda?> getOne(String id) async {
    return await firebaseConnection.getOneInCollection(shop_collection, id);
  }

  @override
  Future<void> update(String id, Tienda t) async {
    return await firebaseConnection.update(shop_collection, id, t.toJson());
  }

  Future<Tienda> getByUserId(UserModel user) async {
    String userId = user.ref;
    var response;
    var exists =
        await firebaseConnection.getOneInCollection(shop_collection, userId);
    if (exists != null) {
      response = Tienda.fromJson(exists);
    } else {
      var tienda = Tienda(
          ref: userId, name: 'Name', latitude: 4.63156, longitude: -74.067408);
      var response = await this.update(userId, tienda);
      response = tienda;
    }

    if (user.store == null) {
      user.store = userId;
      print(user.toJson());
      await _user_repository.update(userId, user);
    }

    return response;
  }
}
