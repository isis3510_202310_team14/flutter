abstract class IRepository<T> {
  Future<List> getAll();
  Future<T?> getOne(String id);
  Future<String> add(T t);
  Future<void> update(String id, T t);
  Future<void> delete(String id);
}