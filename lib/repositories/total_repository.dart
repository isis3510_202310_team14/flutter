import 'package:bidit/repositories/IRepositoryFirebase.dart';
import 'package:bidit/services/firestore_service.dart';

class TotalRepository extends IRepository{

  var total_collection = 'totalized';
  FirebaseConnection firebaseConnection = FirebaseConnection();

  @override
  Future<String> add(t) {
    // TODO: implement add
    throw UnimplementedError();
  }

  @override
  Future<void> delete(String id) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<List> getAll() async {
    return await firebaseConnection.getCollection(total_collection);
  }

  @override
  Future getOne(String id) {
    // TODO: implement getOne
    throw UnimplementedError();
  }

  Future<void> updateOnly(t) async {
    await firebaseConnection.update(total_collection, 'totalDocument', t);
  }
  
  @override
  Future<void> update(String id, t) {
    // TODO: implement update
    throw UnimplementedError();
  }
  
}