import 'package:bidit/model/user_model.dart';
import 'package:bidit/repositories/IRepositoryFirebase.dart';
import 'package:bidit/services/firestore_service.dart';

class UserRepository implements IRepository<UserModel> {
  var user_collection = 'users';

  FirebaseConnection firebaseConnection = FirebaseConnection();

  @override
  Future<String> add(UserModel t) async {
    return await firebaseConnection.addToCollection(
        user_collection, t.toJson());
  }

  @override
  Future<void> delete(String id) async {
    return await firebaseConnection.delete(user_collection, id);
  }

  @override
  Future<List> getAll() async {
    return await firebaseConnection.getCollection(user_collection);
  }

  Future<List> getAllByUser(userRef) async {
    return await firebaseConnection.getCollectionByUser(
        user_collection, userRef);
  }

  Future<UserModel> getById(userId) async {
    var response;
    var exists =
        await firebaseConnection.getOneInCollection(user_collection, userId);
    if (exists != null) {
      response = UserModel.fromJson(exists);
    } else {
      var user = UserModel(
          ref: userId,
          nombre: 'nombre',
          apellido: 'apellido',
          edad: 18,
          email: 'https://ui-avatars.com/api/?background=random',
          documento: 123);
      var response = await this.update(userId, user);
      response = user;
    }

    return response;
  }

  @override
  Future<UserModel> getOne(String id) async {
    final data = UserModel.fromJson(
        await firebaseConnection.getOneInCollection(user_collection, id));
    return data;
  }

  @override
  Future<void> update(String id, UserModel t) async {
    var response =
        await firebaseConnection.update(user_collection, id, t.toJson());
    return response;
  }
}
