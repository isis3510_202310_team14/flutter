import 'package:bidit/model/prenda_model.dart';
import 'package:bidit/repositories/IRepositoryFirebase.dart';
import 'package:bidit/services/firestore_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PrendaRepository implements IRepository<Prenda> {

  var prenda_collection = 'clothes';

  FirebaseConnection firebaseConnection = FirebaseConnection();

  @override
  Future<String> add(Prenda t) async {
    return await firebaseConnection.addToCollection(prenda_collection, t.toJson());
  }

  @override
  Future<void> delete(String id) async {
    return await firebaseConnection.delete(prenda_collection, id);
  }

  @override
  Future<List> getAll() async {
    return await firebaseConnection.getCollection(prenda_collection);
  }

  Future<List> getAllByUser(userRef) async {
    return await firebaseConnection.getCollectionByUser(prenda_collection, userRef);
  }

  @override
  Future<Prenda> getOne(String id) async {
    final data = Prenda.fromJson(await firebaseConnection.getOneInCollection(prenda_collection, id));
    return data;
  }

  @override
  Future<void> update(String id, Prenda t) async {
    return await firebaseConnection.update(prenda_collection, id, t.toJson());
  }

}