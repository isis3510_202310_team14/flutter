import 'package:bidit/model/prenda_model.dart';
import 'package:bidit/model/wishlist_model.dart';
import 'package:bidit/repositories/IRepositoryFirebase.dart';
import 'package:bidit/services/firestore_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class WishlistRepository implements IRepository<Wishlist> {

  var wishlist_collection = 'wishlist';

  FirebaseConnection firebaseConnection = FirebaseConnection();

  @override
  Future<String> add(Wishlist t) async {
    return await firebaseConnection.addToCollection(wishlist_collection, t.toJson());
  }

  @override
  Future<void> delete(String id) async {
    return await firebaseConnection.delete(wishlist_collection, id);
  }

  @override
  Future<List> getAll() async {
    return await firebaseConnection.getCollection(wishlist_collection);
  }

  Future<List> getAllByUser(userRef) async {
    List data = await firebaseConnection.getCollectionByUser(wishlist_collection, userRef);
    print(data);
    return data;
  }

  @override
  Future<Wishlist> getOne(String id) async {
    final data = Wishlist.fromJson(await firebaseConnection.getOneInCollection(wishlist_collection, id));
    return data;
  }

  @override
  Future<void> update(String id, Wishlist t) async {
    return await firebaseConnection.update(wishlist_collection, id, t.toJson());
  }

}