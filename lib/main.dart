import 'package:bidit/layout.dart';
import 'package:bidit/services/notification_services.dart';
import 'package:bidit/views/lista-prendas-propias.dart';
import 'package:bidit/views/profile-store.dart';
import 'package:bidit/views/settings.dart';
import 'package:bidit/views/wishlist.dart';
import 'views/create-auction.dart';
import 'package:bidit/views/lista-prendas.dart';
import 'package:bidit/views/login.dart';
import 'package:bidit/views/profile.dart';
import 'package:bidit/views/register.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:bidit/views/init.dart';
import 'package:bidit/views/login.dart';
import 'package:bidit/views/register.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'views/map.dart';

import 'views/auction-profile.dart';
import 'layout.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseFirestore.instance.settings =
      const Settings(persistenceEnabled: true);
  await NotificationService().setup();

  final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
  final FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  runApp(MaterialApp(
    theme: new ThemeData(
        scaffoldBackgroundColor: const Color.fromRGBO(16, 64, 59, 100)),
    initialRoute: '/init',
    navigatorObservers: [FirebaseAnalyticsObserver(analytics: analytics)],
    routes: {
      '/init': (context) => const InitialScreen(),
      '/nav': (BuildContext context) => BiditNav(),
      '/login': (context) => const LoginScreen(),
      '/register': (context) => const RegisterScreen(),
      '/list': (context) => const ListaPrendasScreen(),
      '/profile': (context) => const ProfileScreen(),
      '/create': (context) => const CreateAuctionScreen(),
      '/map': (context) => const MapScreen(),
      '/auction': (context) => ProfileAuctionScreen(
            prendaId: '',
          ),
      '/settings': (context) => const SettingsScrenn(),
      '/store': (context) => const ProfileStoreScreen(),
      '/my-auctions': (context) => const ListaPrendasPropiasScreen(),
      '/wishlist': (context) => WishlistScreen(
            userId: '',
          ),
    },
  ));
}
