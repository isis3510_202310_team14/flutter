import 'package:bidit/views/create-auction.dart';
import 'package:bidit/views/lista-prendas.dart';
import 'package:bidit/views/map.dart';
import 'package:bidit/views/profile.dart';
import 'package:flutter/material.dart';
import 'package:bidit/views/init.dart';



class BiditNav extends StatefulWidget {
  const BiditNav({super.key});

  @override
  State<BiditNav> createState() => _BiditNavState();
}

class _BiditNavState extends State<BiditNav> {
  int indexTap = 0;
  final List<Widget> widgetsChildren = [
    ListaPrendasScreen(),
    MapScreen(),
    ProfileScreen(),
    CreateAuctionScreen()
  ];

  void onTapTapped(int index) {
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: 
      Theme(
        data: Theme.of(context).copyWith(
            canvasColor: const Color.fromRGBO(16, 64, 59, 100),),
        child: BottomNavigationBar(
            selectedItemColor: const Color.fromRGBO(138, 166, 163, 100),
            onTap: onTapTapped,
            type: BottomNavigationBarType.fixed,
            currentIndex: indexTap,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: ""),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: ""),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: ""),
              BottomNavigationBarItem(icon: Icon(Icons.add), label: "")
            ]),
      ),
    );
  }
}
