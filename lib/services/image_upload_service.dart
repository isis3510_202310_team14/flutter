import 'dart:ffi';
import 'dart:io';
import 'dart:math';

import 'package:bidit/model/image_model.dart';
import 'package:bidit/model/tienda_model.dart';
import 'package:bidit/model/user_model.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ImageService {
  Future<ImageModel> upload(ImageModel _params, UserModel user) async {
    ImageModel response = _params;
    final storage = FirebaseStorage.instance;

    var status_camera = await Permission.camera.status;
    if (status_camera.isDenied) {
      await Permission.camera.request();
      status_camera = await Permission.camera.status;
    }
    var status_photos = await Permission.photos.status;
    if (status_photos.isDenied) {
      await Permission.photos.request();
      status_photos = await Permission.photos.status;
    }

    if (status_photos.isGranted) {
      response = await picker(response, user);
    } else if (status_camera.isGranted) {
      response = await picker(response, user);
    } else {
      response.setError('We do not have permissions to access images');
      response.setHasError(true);
    }

    if (response.hasLink) {
      print(response.link);
    }

    return response;
  }

  Future<ImageModel> picker(ImageModel _params, UserModel user) async {
    try {
      ImagePicker imagePicker = ImagePicker();
      XFile? file = await imagePicker.pickImage(source: ImageSource.gallery);

      if (file == null) {
        _params.setError('Invalid selection');
        _params.setHasError(true);
        return _params;
      }

      //Import dart:core
      String uniqueFileName = user.ref;

      Reference referenceRoot = FirebaseStorage.instance.ref();
      Reference referenceDirImages = referenceRoot.child('images');

      //Create a reference for the image to be stored
      Reference referenceImageToUpload =
          referenceDirImages.child(uniqueFileName);
      //Store the file
      await referenceImageToUpload.putFile(File(file!.path));
      //Success: get the download URL
      var imageUrl = await referenceImageToUpload.getDownloadURL();
      _params.setLink(imageUrl);
      _params.setHasLink(true);
      return _params;
    } catch (e) {
      _params.setError('There was an error while uploading');
      _params.setHasError(true);
      return _params;
    }
  }

  Future<ImageModel> uploadTienda(ImageModel _params, Tienda user) async {
    ImageModel response = _params;
    final storage = FirebaseStorage.instance;

    var status_camera = await Permission.camera.status;
    if (status_camera.isDenied) {
      await Permission.camera.request();
      status_camera = await Permission.camera.status;
    }
    var status_photos = await Permission.photos.status;
    if (status_photos.isDenied) {
      await Permission.photos.request();
      status_photos = await Permission.photos.status;
    }

    if (status_photos.isGranted) {
      response = await pickerTienda(response, user);
    } else if (status_camera.isGranted) {
      response = await pickerTienda(response, user);
    } else {
      response.setError('We do not have permissions to access images');
      response.setHasError(true);
    }

    if (response.hasLink) {
      print(response.link);
    }

    return response;
  }

  Future<ImageModel> pickerTienda(ImageModel _params, Tienda user) async {
    try {
      ImagePicker imagePicker = ImagePicker();
      XFile? file = await imagePicker.pickImage(source: ImageSource.gallery);

      if (file == null) {
        _params.setError('Invalid selection');
        _params.setHasError(true);
        return _params;
      }

      //Import dart:core
      String uniqueFileName = Random().nextInt(10000).toString();

      Reference referenceRoot = FirebaseStorage.instance.ref();
      Reference referenceDirImages = referenceRoot.child('images');

      //Create a reference for the image to be stored
      Reference referenceImageToUpload =
          referenceDirImages.child(uniqueFileName);
      //Store the file
      await referenceImageToUpload.putFile(File(file!.path));
      //Success: get the download URL
      var imageUrl = await referenceImageToUpload.getDownloadURL();
      _params.setLink(imageUrl);
      _params.setHasLink(true);
      return _params;
    } catch (e) {
      _params.setError('There was an error while uploading');
      _params.setHasError(true);
      return _params;
    }
  }
}
