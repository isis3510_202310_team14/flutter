import 'dart:io';
import 'dart:isolate';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class ImageClothesService {

  Future<File> compressImage(Map map) async {
    final SendPort port = map['port'];
    final File file = map['file'];
    final String targetPath = map['target'];

    final filePath = file.absolute.path;

    File compressedFile = await FlutterNativeImage.compressImage(file.path,
    quality: 75 );
    return compressedFile;
  }


  Future<String> upload() async {
    var status_camera = await Permission.camera.status;
    if (status_camera.isDenied) {
      await Permission.camera.request();
      status_camera = await Permission.camera.status;
    }
    var status_photos = await Permission.photos.status;
    if (status_photos.isDenied) {
      await Permission.photos.request();
      status_photos = await Permission.photos.status;
    }

    File archivo;

    if (status_photos.isGranted) {
      archivo = await picker();
      return uploadStorage(archivo);

    } else if (status_camera.isGranted) {
      archivo = await picker();
      return uploadStorage(archivo);
    } else {
      return 'We do not have permissions to access images';
    }
  }

  Future<String> uploadStorage(File file) async {
    Reference referenceRoot = FirebaseStorage.instance.ref();
    Reference referenceDirImages = referenceRoot.child('images');
    Reference referenceImageToUpload =
          referenceDirImages.child((DateTime.now().millisecondsSinceEpoch / 1000).round().toString());
    await referenceImageToUpload.putFile(file);
    var imageUrl = await referenceImageToUpload.getDownloadURL();
    return imageUrl;
  }

  Future<File> picker() async {
      ImagePicker imagePicker = ImagePicker();
      XFile? file = await imagePicker.pickImage(source: ImageSource.gallery);
      final p = ReceivePort();
      final data = {'port': p.sendPort, 'file' : File(file!.path), 'target': file!.path};
      File image = await compressImage(data);
      return image;
  }
}