import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tzData;

class NotificationService {
  final _localNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future<void> setup() async {
    const iosSetting = DarwinInitializationSettings();

    const initSettings = InitializationSettings(iOS: iosSetting);

    await _localNotificationsPlugin.initialize(initSettings);
  }

  void addNotification(String title, String body) async {
    
    final iosDetail = DarwinNotificationDetails();

    final noticeDetail = NotificationDetails(
      iOS: iosDetail,
    );

    final id = 0;

    await _localNotificationsPlugin.show(id, title, body, noticeDetail);
  }
}
