import 'package:firebase_auth/firebase_auth.dart';
import 'package:bidit/model/user_model.dart';

class AuthService {
  Future<bool> register(user) async {
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: user.getEmail, password: user.getPassword);
    await FirebaseAuth.instance.currentUser
        ?.updateDisplayName("$user.getNombre $user.getApellido");
    return true;
  }

  Future<bool> login(username, password) async {
    final credential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: username, password: password);
    return true;
  }

   Future<String?> getUserId() async {
    return FirebaseAuth.instance.currentUser?.uid;
  }

}