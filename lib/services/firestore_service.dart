import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseConnection {
  final firestore = FirebaseFirestore.instance;

  Future<List> getCollection(collection) async {
    List data = [];
    CollectionReference collectionReference = firestore.collection(collection);

    QuerySnapshot queryPeople = await collectionReference.get();

    for(int i=0; i<queryPeople.docs.length; i++){
      var element = queryPeople.docs[i];
      data.add(element.data());
    }
    return data;
  }

  Future<List> getCollectionByUser(collection, userRef) async {
    print(userRef);
    List data = [];
    final collectionReference = await firestore
        .collection(collection)
        .where("userRef", isEqualTo: userRef)
        .get();
    var element;
    for (int i=0; i<collectionReference.docs.length; i++){
      element = collectionReference.docs[i];
      data.add(element.data());
    }
    print(data);
    return data;
  }

  Future<dynamic> getOneInCollection(collection, docRef) async {
    var aux = await firestore.collection(collection).doc(docRef).get();
    return aux.data();
  }

  Future<String> addToCollection(collection, data) async {
    DocumentReference docRef = await firestore.collection(collection).add(data);
    return docRef.id;
  }

  Future<void> update(collection, docRef, data) async {
    await firestore.collection(collection).doc(docRef).set(data);
  }

  Future<void> delete(collection, docRef) async {
    await firestore.collection(collection).doc(docRef).delete();
  }
}
