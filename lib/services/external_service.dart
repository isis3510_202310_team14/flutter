import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ExternalService {
  Future<dynamic> fetchPrices() async {
    final response = await http.get(
        Uri.parse('http://159.89.181.20:5001/api/service/price-by-category'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return jsonDecode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}
