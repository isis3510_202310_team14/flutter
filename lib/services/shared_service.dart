import 'package:shared_preferences/shared_preferences.dart';

class SharedService {

  void setBool(key, value) async{
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool(key, value);
  }

  void setDouble(key, value) async{
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setDouble(key, value);
  }

  void setInt(key, value) async{
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setInt(key, value);
  }

  void setString(key, value) async{
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString(key, value);
  }

  void setStringList(key, value) async{
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setStringList(key, value);
  }

  Future<bool?> getBool(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getBool(key);
  }

  Future<double?> getDouble(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getDouble(key);
  }

  Future<int?> getInt(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getInt(key);
  }

  Future<String?> getString(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(key);
  }

  Future<List<String>?> getStringList(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getStringList(key);
  }

  Future<dynamic> get(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.get(key);
  }
  void remove(String key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.remove(key);
  }
}
