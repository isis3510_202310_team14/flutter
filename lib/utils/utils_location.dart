import 'dart:math' as math;

class LocationUtils{

  double degreesToRadians(degrees) {
    return degrees * math.pi / 180;
  }

  Future<double> getDistanceBetweenPoints(double lat1, double lat2, double lon1, double lon2) async{
    double earthRadius = 6371;

    double dLat = degreesToRadians(lat2-lat1);
    double dLon = degreesToRadians(lon2-lon1);

    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);
    var a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(lat1) * math.cos(lat2); 
    var c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a)); 
    return earthRadius * c;
  }

}